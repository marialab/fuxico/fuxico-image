menu:
	@echo "all => projectpull clean frontupdate test"
	@echo "setup => projectclone projectpull frontupdate test"
	@echo "projectclone => clone de todos os projetos para o diretorio atual"
	@echo "projectpull => update de todos os projetos do diretório atual"
	@echo "frontupdate => copia do front-end para o core"
	@echo "test => Docker Compose Up"
	@echo "clean => Clean make createimage stuffs"
	
.DEFAULT_GOAL = menu


all: projectpull clean frontupdate test

setup: projectclone projectpull frontupdate test

projectclone:
	git clone git@gitlab.com:marialab/fuxico/fuxico-core.git ../fuxico-core ;\
	git clone git@gitlab.com:marialab/fuxico/fuxico-front.git ../fuxico-front ;\
	git clone git@gitlab.com:marialab/fuxico/wizard.git ../wizard ;\
	git clone git@gitlab.com:marialab/fuxico/fuxico-backend.git ../fuxico-backend ;\

projectcloneci:
	git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/marialab/fuxico/fuxico-core.git ../fuxico-core ;\
	git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/marialab/fuxico/fuxico-front.git ../fuxico-front ;\
	git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/marialab/fuxico/wizard.git ../wizard ;\
	git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/marialab/fuxico/fuxico-backend.git ../fuxico-backend ;\

projectpull:
	#tratar erro caso não esteja na master
	cd ../fuxico-front && git pull -r --autostash; \
	cd ../fuxico-core && git pull -r --autostash; \
	cd ../wizard && git pull -r --autostash; \
	cd ../fuxico-backend && git pull -r --autostash; \
	cd ../fuxico-image

frontupdate:
	cp -r ../fuxico-front/public/* ../fuxico-core/piratebox/www/
	cp -r ../fuxico-front/share/* ../fuxico-core/piratebox/share/
	cd ../wizard && \
		npm install && \
		npm run build && \
		mkdir -p ../fuxico-core/piratebox/www/install && \
		cp -r dist/* ../fuxico-core/piratebox/www/install
	cd -

test: frontupdate
	$(MAKE) -C ../fuxico-core package
	sudo docker-compose up --build -d

createimage: clean frontupdate
	$(MAKE) -C ../fuxico-core all
	cd -

clean:
	$(MAKE) -i -C ../fuxico-core clean
	cd -


.PHONY: all projectclone projectpull frontupdate test createimage clean
