# fuxico-image

Instalando o docker: [[https://docs.docker.com/install/]]

Para executar:

1. Edite o arquivo `/etc/hosts` da maquina adicionando a linha `127.0.0.1     fuxico.lan`
2. Na linha de comando, execute o comando `make all` para clonar os repositorios, atualiza-los, copiar os estaticos para o core e subir o container docker.
3. Para manipular os arquivos estaticos em tempo de execução, altere no diretório `fuxico-core/piratebox/www_content` e `fuxico-core/piratebox/share`

## References

- https://devsidestory.com/build-a-64-bit-kernel-for-your-raspberry-pi-3/